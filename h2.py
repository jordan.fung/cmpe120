def convertToLower(a):
    return chr(ord(a)+32)
def convertToUpper(a):
    return chr(ord(a)-32)
def isInAlphabet(a):
    return (40<ord(a)<123)
def isDigit(a):
    return (47<ord(a)<58)
def isSpecial(a):
    return not(isInAlphabet(a) or isDigit(a))