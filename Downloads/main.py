import threading
import time

def threadCountToTen():
    for x in range(0,10):
        print(x)
        x+=1
        time.sleep(1)


def threadCountDown():
    i=10
    while i>0:
        print(i)
        i-=1
        time.sleep(1)

def main():
    a = threading.Thread(target=threadCountToTen)
    b = threading.Thread(target=threadCountDown)
    a.start()
    a.join()
    b.start()
    b.join()

if __name__=="__main__":
    main()
